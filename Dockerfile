FROM mhart/alpine-node:latest

RUN mkdir -p /usr/src/knowledgebase-react-api

# Switch working space to new one
WORKDIR /usr/src/knowledgebase-react-api

COPY package.json /usr/src/knowledgebase-react-api

# Install Yarn package
RUN npm install yarn -g

# Install all the packages
RUN yarn install

# Copy all the source files to folder
COPY . /usr/src/knowledgebase-react-api

EXPOSE 7010

CMD ["npm", "start"]






