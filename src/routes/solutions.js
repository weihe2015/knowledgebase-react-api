import express from 'express';
import fs from 'fs-extra';
import path from 'path';
import _ from 'lodash';
import multer from 'multer';
import moment from 'moment';
import zipFolder from 'zip-folder';
import redis from 'redis';
import bluebird from 'bluebird';

import User from '../models/User';
import authenticate from '../middlewares/authenticate';
import Solution from '../models/Solution';
import SolutionCounter from '../models/SolutionCounter';
import parseErrors from '../utils/parseErrors';
import {
	sendUnApprovedSolutionEmail,
	sendApprovalSolutionEmail,
	sendRejectSolutionEmail,
	sendDeleteSolutionEmail
} from '../mailer';


const router = express.Router();
router.use(authenticate);

// make redis using Promise
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

// create a new client for redis
const client = redis.createClient({ "host": "redis" || "127.0.0.1" });

const upload = multer({
	dest: './uploads',
	storage: multer.memoryStorage(),
	// file size limitation in bytes
	limits: { fileSize: 52428800 }
});

// Fetch all solutions if the query is empty. 
// search solutions by title and content.
// First find in Redis by query, if there it is, return, if not, search in the database:
// The result in redis will expire in one day:
router.get("/", (req, res) => {
	const query = req.query.q;
        if (query === undefined || query.length === 0) {
		// If user type is client, return only approved and public visibility solutions:
		const userType = req.query.user;
                if (userType !== undefined && userType === 'client') {
			// check if client's solutions is in redis:
			client.getAsync('allClientSolutions').then(cachedResult => {
				// Solutions are note in cache or expired:
				if (!cachedResult) {
					// fetch only approved and public visibility solutions:
					Solution.find({ status: 'Approved', visibility: 'public'},
						{title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1})
					.then(solutions => {
						client.setex('allClientSolutions',
							process.env.CACHE_TIME, JSON.stringify(solutions));
						res.status(200).json({ solutions });
					})
					.catch(err => {
						res.status(404).json({ errors: { global: "No solutions is found." } });
					})
				}
				// Solutions are in cached, returned
				else {
					res.status(200).json({ solutions: JSON.parse(cachedResult) });
				}
			})
			.catch(err => res.status(400).json({ errors: { global: "Cached System Error." } }));
		}
		else {
			// check if solution is in Redis:
			client.getAsync('allSolutions').then((cachedResult) => {
				// solutions are not in cache or expired
				if (!cachedResult) {
					// Change to only fetching solutions' id, title, status and visibility:
					Solution.find({},{title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1})
						.then(solutions => {
							client.setex('allSolutions',
								process.env.CACHE_TIME, JSON.stringify(solutions));
							res.status(200).json({ solutions });
						}
					).catch(err => {
						res.status(404).json({ errors: { global: "No solutions is found." } });
					})
				}
				// Solutions are in cache, returned.
				else {
					res.status(200).json({ solutions: JSON.parse(cachedResult) });
				}
			})
			.catch(err => res.status(400).json({ errors: { global: "Cached System Error." } }));
        	}
	}
	else {
		// check if solution is in Redis:
		client.getAsync(query).then((cachedResult) => {
		// solutions are not in cache or expired
		if (!cachedResult) {
			Solution.find({ $text: { $search: query }})
				.then(solutions => {
					if (solutions) {
						client.setex(query, process.env.CACHE_TIME, JSON.stringify(solutions));
						res.status(200).json({ solutions });
					}
					else {
						res.status(200).json({});
					}
				})
		}
		// Solutions are in cache, returned.
		else {
			res.status(200).json({ solutions: JSON.parse(cachedResult) });
		}
		})
		.catch(err => res.status(400).json({
			errors: { global: "Cached System Error in search solutions by query." }}
		));
	}
});

// create new solution
router.post('/', (req, res) => {
	const username = req.currentUser.username;
	const fullName = req.currentUser.firstName + " " + req.currentUser.lastName;
	const updatedSolution = req.body.solution;

	let historyObject = {
		author: username,
		type: 'user',
		header: 'Submission creation',
		meta: `Status: ${updatedSolution.status}`,
		content: `Created by ${fullName} on `,
		time: moment().format("YYYY-MM-DD hh:mm:ss.SSS A")
	};
	updatedSolution.history.unshift(historyObject);

	historyObject = {
		author: 'System',
		type: 'system',
		header: 'Submission Creation Notification',
		meta: `Status: ${updatedSolution.status}`,
		content: `To all managers and team leads
			\n\n Solution:${updatedSolution.solutionId} \n\n
			under ${updatedSolution.topic}, status: Draft on `,
		time: moment().add(1, 'milliseconds').format("YYYY-MM-DD hh:mm:ss.SSS A")
	}
	updatedSolution.history.unshift(historyObject);
	
	// Get solutionId:
	SolutionCounter.findOne().then(data => {
		const newSolutionId = data.seq;
		updatedSolution.solutionId = newSolutionId;
		
		Solution.create({
			...updatedSolution,
			userId: req.currentUser._id,
			createdUsername: fullName,
			updatedUsername: fullName
		}).then(solution => {
	
			// Send UnApproved Solution Email to all managers and team lead:
			User.find({ $and: [{ role: process.env.SEND_EMAIL_ROLE }, { notification: true }] })
				.then(users => {
					_.map(users, async (user) => {
						await sendUnApprovedSolutionEmail(user, solution);
					});
				});
	
			// Reset all solutions key in cache because a new solution is created.
			Solution.find({},{ title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1 }).then(solutions => {
				client.setex('allSolutions',
					process.env.CACHE_TIME, JSON.stringify(solutions));
			});
	
			// update solution Id by 1
			SolutionCounter.update({ 
				solutionId: process.env.SOLUTION_ID},{$inc: { seq: 1 }})
				.then((data) => res.status(200).json({ solution }))
		}).catch(err => res.status(400).json({ errors: parseErrors(err.errors) }));
	})
	.catch(err => res.status(400).json({ errors: parseErrors(err.errors)} ));

})

// get solution by solution id and title
router.get('/:solutionId/', (req, res) => {	 
	const { solutionId } = req.params;
	const title = req.query.t;
	// If title is undefine or empty, we fetch solution by id.
	if (!!title) {
		// First find it in redis cache:
		client.getAsync(solutionId).then((cachedResult) => {
			// If this silution is not in cache or expired:
			if (!cachedResult) {
				Solution.findOne({ solutionId },{ files: 0 }).then(solution => {
					if (solution) {
						// Save it in Redis Cache:
						client.setex(solutionId, process.env.CACHE_TIME, JSON.stringify(solution));
						res.status(200).json({ solution });
					}
					else {
						res.status(400).json({ errors: { global: "Invalid solution Id" } })
					}
				})
			}
			// return the solution in cache:
			else {
				res.status(200).json({ solution: JSON.parse(cachedResult) });
			}
		})
		.catch(err => {
			console.log(err);
			res.status(404).json({error: { global: 'Cache System Error'}});
		});
	}
	else {
		// First find it in redis cache:
		client.getAsync(solutionID).then((cachedResult) => {
			if (!cachedResult) {
				Solution.findOne({$and: [{ solutionId }, { title }]}, { files: 0 }).then(solution => {
					if (solution) {
						// Save it in Redis Cache:
						client.setex(solutionId, process.env.CACHE_TIME, JSON.stringify(solution));
						res.status(200).json({ solution });
					}
					else {
						res.status(400).json({ errors: { global: "Invalid solution Id" } })
					}
				})
			}
			// return the solution in cache:
			else {
				res.status(200).json({ solution: JSON.parse(cachedResult) });
			}
		})
		.catch(err => {
			console.log(err);
			res.status(404).json({error: { global: 'Cache System Error'}});
		});
	}
})

// Get solution by status or expiring date
router.get('/category/:viewName', (req, res) => {
	const { viewName } = req.params;
	if (viewName) {
		switch (viewName) {
			case 'AllApprovedSolutions': {
				// check if approved solution is in cache
				client.getAsync('AllApprovedSolutions').then((cachedResult) => {
					// All Approved solutions are not in cache or expired
					if (!cachedResult) {
						Solution.find({ status: 'Approved' },
							{ title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1})
							.then(solutions => {
								if (solutions) {
									client.setex('AllApprovedSolutions',
										process.env.CACHE_TIME, JSON.stringify(solutions));
									res.status(200).json({ solutions });
								}
								else {
									res.status(200).json({});
								}
							})
					}
					else {
						res.status(200).json({ solutions: JSON.parse(cachedResult) });
					}
				})
					.catch(err => res.status(400).json({
						errors:
							{ global: "Cached System Error In Get All Approved Solutions." }
					}));
				break;
			}
			case 'AllUnApprovedSolutions': {
				client.getAsync('AllUnApprovedSolutions').then((cachedResult) => {
					// All Approved solutions are not in cache or expired
					if (!cachedResult) {
						Solution.find({ status: 'Draft' },
							{ title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1})
							.then(solutions => {
								if (solutions) {
									client.setex('AllUnApprovedSolutions',
										process.env.CACHE_TIME, JSON.stringify(solutions));
									res.status(200).json({ solutions });
								}
								else {
									res.status(200).json({});
								}
							})
					}
					else {
						res.status(200).json({ solutions: JSON.parse(cachedResult) });
					}
				})
					.catch(err => res.status(400).json({
						errors:
							{ global: "Cached System Error In Get All UnApproved Solutions." }
					}));
				break;
			}
			case 'RejectedSolutions': {
				client.getAsync('AllRejectedSolutions').then((cachedResult) => {
					// All Approved solutions are not in cache or expired
					if (!cachedResult) {
						Solution.find({ status: 'Rejected' },
							{ title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1})
							.then(solutions => {
								if (solutions) {
									client.setex('AllRejectedSolutions',
										process.env.CACHE_TIME, JSON.stringify(solutions));
									res.status(200).json({ solutions });
								}
								else {
									res.status(200).json({});
								}
							})
					}
					else {
						res.status(200).json({ solutions: JSON.parse(cachedResult) });
					}
				})
					.catch(err => res.status(400).json({
						errors:
							{ global: "Cached System Error In Get All Rejected Solutions." }
					}));
				break;
			}
			case 'ExpiredSolutions': {
				client.getAsync('AllExpiredSolutions').then((cachedResult) => {
					// All Approved solutions are not in cache or expired
					if (!cachedResult) {
						Solution.find({ expirationDate: { "$lt": new Date() } },
							{ title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1})
							.then(solutions => {
								if (solutions) {
									client.setex('AllExpiredSolutions',
										process.env.CACHE_TIME, JSON.stringify(solutions));
									res.status(200).json({ solutions });
								}
								else {
									res.status(200).json({});
								}
							})
					}
					else {
						res.status(200).json({ solutions: JSON.parse(cachedResult) });
					}
				})
					.catch(err => res.status(400).json({
						errors:
							{ global: "Cached System Error In Get All Expired Solutions." }
					}));
				break;
			}
			case 'ExpiringSolutions': {
				client.getAsync('AllExpiringSolutions').then((cachedResult) => {
					// All Approved solutions are not in cache or expired
					if (!cachedResult) {
						Solution.find({
							$or: [
								{ expirationDate: null },
								{ expirationDate: { "$gte": new Date() } },
							]
						},{ title: 1, solutionId: 1, status: 1, visibility: 1})
							.then(solutions => {
								if (solutions) {
									client.setex('AllExpiringSolutions',
										process.env.CACHE_TIME, JSON.stringify(solutions));
									res.status(200).json({ solutions });
								}
								else {
									res.status(200).json({});
								}
							})
					}
					else {
						res.status(200).json({ solutions: JSON.parse(cachedResult) });
					}
				})
					.catch(err => res.status(400).json({
						errors:
							{ global: "Cached System Error In Get All Expiring Solutions." }
					}));
				break;
			}
			default: {
				// check if solution is in Redis:
				client.getAsync('allSolutions').then((cachedResult) => {
					// solutions are not in cache or expired
					if (!cachedResult) {
						Solution.find({},{title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1 })
						.then(solutions => {
							client.setex('allSolutions',
								process.env.CACHE_TIME, JSON.stringify(solutions));
							res.status(200).json({ solutions });
						})
					}
					// Solutions are in cache, returned.
					else {
						res.status(200).json({ solutions: JSON.parse(cachedResult) });
					}
				})
					.catch(err => res.status(400).json({
						errors:
							{ global: "Cached System Error In Get All Solutions By Category." }
					}));
				break;
			}
		}
	}
	else {
		// check if solution is in Redis:
		client.getAsync('allSolutions').then((cachedResult) => {
			// solutions are not in cache or expired
			if (!cachedResult) {
				Solution.find({},{ title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1 })
				.then(solutions => {
					client.setex('allSolutions',
						process.env.CACHE_TIME, JSON.stringify(solutions));
					res.status(200).json({ solutions });
				})
			}
			// Solutions are in cache, returned.
			else {
				res.status(200).json({ solutions: JSON.parse(cachedResult) });
			}
		})
			.catch(err => res.status(400).json({
				errors:
					{ global: "Cached System Error in no view name matching." }
			}));
	}
})

// Update solution by incrementing viewNumber
router.put('/:solutionId/viewnumber', (req, res) => {
	let { solution } = req.body;
	const { solutionId } = req.params;

	Solution.findOneAndUpdate({ solutionId }, { $inc: { viewNumber: 1 } })
		.then((solution) => {
			solution.viewNumber++;
			// update the solution in cache:
			client.setex(solutionId, process.env.CACHE_TIME, JSON.stringify(solution));
			// Reset all solutions key in cache
			// because solution is updated successfully.
			Solution.find({},{ title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1})
			.then(solutions => {
				client.setex('allSolutions', process.env.CACHE_TIME, JSON.stringify(solutions));
			})
				.then(() => res.status(200).json({ solution }));
		})
		.catch(err => {
			res.status(400).json({ errors: { global: "Incrementing solution viewNumber error" }})	
		});
})

// Check if this user has voted previously:
router.get('/:solutionId/rating', (req, res) => {
	const { solutionId } = req.params;
	const { email } = req.query;
	// Find userId by email:
	User.findOne({ email }).then(user => {
		if (user) {
			const userId = user._id;
			Solution.findOne({ solutionId }).then(solution => {
				if (solution) {
					if (solution.ratingUserId.includes(userId.toString())) {
						res.status(200).json({ status: true });
					}
					else {
						res.status(200).json({ status: false });	
					}
				}
				else {
					res.status(400).json({ errors: { global: "Invalid solutionId for checking whether this user has voted" }});
				}
			})
		}
		else {
			res.status(400).json({ errors: { global: "Invalid email for checking whether this user has voted" }});
		}
	})
})

// Update solution by incrementing ratingUp
router.put('/:solutionId/ratingup', (req, res) => {
	const { solutionId } = req.params;
	const { email } = req.query;
	// Find userId by email:
	User.findOne({ email }).then(user => {
		if (user) {
			const userId = user._id;
			Solution.findOneAndUpdate({ solutionId }, { $inc: { ratingUp: 1 }, $push: { ratingUserId: userId.toString() } })
				.then((solution) => {
					solution.ratingUp++;
					if (!solution.hasOwnProperty('ratingUserId')) {
						solution.ratingUserId = [];
					}
					solution.ratingUserId.push(userId);
					// update the solution in cache:
					client.setex(solutionId, process.env.CACHE_TIME, JSON.stringify(solution));
					Solution.find({},{ title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1}).then(solutions => {
						client.setex('allSolutions', process.env.CACHE_TIME, JSON.stringify(solutions));
					})
					.then(() => res.status(200).json({ solution }));
				})
			.catch(err => {
				res.status(400).json({ errors: { global: `Incrementing solution rating up error.` } });
			});
		}
		else {
			res.status(400).json({ errors: { global: `Failed to find user by email in changing solution rating up.` } });
		}
	})	
})

// Update solution by incrementing ratingDown
router.put('/:solutionId/ratingdown/', (req, res) => {
	const { solutionId } = req.params;
	const { email } = req.query;
	// Find userId by email:
	User.findOne({ email }).then(user => {
		if (user) {
			const userId = user._id;
			Solution.findOneAndUpdate({ solutionId }, { $inc: { ratingDown: 1 }, $push: { ratingUserId: userId.toString() } })
				.then((solution) => {
					solution.ratingDown++;
					if (!solution.hasOwnProperty('ratingUserId')) {
						solution.ratingUserId = [];
					}
					solution.ratingUserId.push(userId);
					// update the solution in cache:
					client.setex(solutionId, process.env.CACHE_TIME, JSON.stringify(solution));
					Solution.find({},{ title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1}).then(solutions => {
						client.setex('allSolutions', process.env.CACHE_TIME, JSON.stringify(solutions));
					})
					.then(() => res.status(200).json({ solution }));
				})
			.catch(err => {
				console.log(err);
				res.status(400).json({ errors: { global: `Incrementing solution rating down error.` } });
			});
		}
		else {
			res.status(400).json({ errors: { global: `Failed to find user by email in changing solution rating down.` } });
		}
	});
});

// Update solution by changing status to Approved
router.put('/:solutionId/approval', (req, res) => {
	const fullName = req.currentUser.firstName + " " + req.currentUser.lastName;
	const approvalComment = req.body.approvalComment;
	const { solutionId } = req.params;

	let updatedSolution = req.body.solution;
	if (fullName.length > 0) {
		updatedSolution.updatedUsername = fullName;
	}

	updatedSolution.status = "Approved";
	let historyObject = {
		author: fullName,
		header: 'Submission Approval',
		type: 'user',
		meta: `Status: Approved`,
		content: `Approved by ${fullName} on `,
		time: moment()
	};
	updatedSolution.history.unshift(historyObject);

	historyObject = {
		author: 'System',
		type: 'system',
		header: 'Submission Approval Notification',
		meta: `Status: Approved`,
		content: `To all managers and team leads
			\n\n Solution:${updatedSolution.solutionId} \n\n under
			${updatedSolution.topic}, status: Approved, on `,
		time: moment().add(1, 'milliseconds').format("YYYY-MM-DD hh:mm:ss.SSS A")
	}
	updatedSolution.history.unshift(historyObject);

	Solution.findOne({ solutionId }).then(solution => {
		if (solution) {
			// update the solution in cache:
			client.setex(solutionId, process.env.CACHE_TIME, JSON.stringify(solution));

			updatedSolution.files = solution.files;
			Solution.update({ solutionId }, { $set: updatedSolution }).then(() => {
				// update the solution in cache:
				client.setex(solutionId, process.env.CACHE_TIME, JSON.stringify(updatedSolution));
				// remove the attach file buffer
				updatedSolution.files.forEach((info) => { delete info.data });

				// Send Approval email to admin with email notification enabled for notifying approval:
				User.find({ $and: [{ role: process.env.SEND_EMAIL_ROLE }, { notification: true }] })
					.then(users => {
						_.map(users, async (user) => {
							await sendApprovalSolutionEmail(user, updatedSolution, approvalComment);
						});
					});
				
				// TODO: Send the confirmation email and comment back to the solution creation user.

				// Reset all solutions key in cache
				// because solution is updated successfully.
				Solution.find({},{ title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1})
				.then(solutions => {
					client.setex('allSolutions', process.env.CACHE_TIME, JSON.stringify(solutions));
				})
					.then(() => res.status(200).json({ solution: updatedSolution }));

			}).catch(err => res.status(400).json({ errors: { global: "Solution Approval Update error" } }));
		}
		else {
			res.status(400).json({ errors: { global: "no solution is found" } })
		}
	})
		.catch(err => {
			res.status(400).json({ errors: { global: "Incorrect solution Id" } });
		});

})

// Update solution by changing status to Rejected
router.put('/:solutionId/rejection', (req, res) => {
	const fullName = req.currentUser.firstName + " " + req.currentUser.lastName;
	const rejectionComment = req.body.rejectionComment;
	const { solutionId } = req.params;

	let updatedSolution = req.body.solution;
	if (fullName.length > 0) {
		updatedSolution.updatedUsername = fullName;
	}

	updatedSolution.status = "Rejected";
	let historyObject = {
		author: fullName,
		header: 'Submission Rejection',
		type: 'user',
		meta: `Status: Rejected`,
		content: `Rejected by ${fullName} on `,
		time: moment().format("YYYY-MM-DD hh:mm:ss.SSS A")
	};
	updatedSolution.history.unshift(historyObject);

	historyObject = {
		author: 'System',
		type: 'system',
		header: 'Submission Rejection Notification',
		meta: `Status: Rejected`,
		content: `To all managers and team leads
			\n\n Solution:${updatedSolution.solutionId} \n\n under
			${updatedSolution.topic}, status: Rejected, on `,
		time: moment().add(1, 'milliseconds').format("YYYY-MM-DD hh:mm:ss.SSS A")
	}
	updatedSolution.history.unshift(historyObject);

	Solution.findOne({ solutionId }).then(solution => {
		if (solution) {
			updatedSolution.files = solution.files;
			Solution.update({ solutionId }, { $set: updatedSolution }).then(() => {
				// update the solution in cache:
				client.setex(solutionId, process.env.CACHE_TIME, JSON.stringify(updatedSolution));

				// remove the attach file buffer
				updatedSolution.files.forEach((info) => { delete info.data });

				// Send reject email to admin with email notification turned on for notifying rejection:
				User.find({ $and: [{ role: process.env.SEND_EMAIL_ROLE }, { notification: true }] })
					.then(users => {
						_.map(users, async (user) => {
							await sendRejectSolutionEmail(user, updatedSolution, rejectionComment);
						});
					});
				// TODO: Send the confirmation email and comment back to the solution creation user.

				// Reset all solutions key in cache
				// because solution is updated successfully.
				Solution.find({},{ title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1})
				.then(solutions => {
					client.setex('allSolutions', process.env.CACHE_TIME, JSON.stringify(solutions));
				})
					.then(() => res.status(200).json({ solution: updatedSolution }));

			}).catch(err => res.status(400).json({ errors: { global: "Solution Rejection Update error" } }));
		}
		else {
			res.status(400).json({ errors: { global: "no solution is found" } })
		}
	})
		.catch(err => {
			res.status(400).json({ errors: { global: "Incorrect solution Id" } });
		});

})

// Update solution by changing status to Draft
router.put('/:solutionId/draft', (req, res) => {
	const fullName = req.currentUser.firstName + " " + req.currentUser.lastName;
	const { solutionId } = req.params;

	let updatedSolution = req.body.solution;
	if (fullName.length > 0) {
		updatedSolution.updatedUsername = fullName;
	}

	updatedSolution.status = "Draft";
	const historyObject = {
		author: fullName,
		type: 'user',
		header: 'Submission Edit',
		meta: `Status: Draft`,
		content: `Edit by ${fullName} on `,
		time: moment().format("YYYY-MM-DD hh:mm:ss.SSS A")
	};
	updatedSolution.history.unshift(historyObject);

	Solution.findOne({ solutionId }).then(solution => {
		if (solution) {
			updatedSolution.files = solution.files;
			Solution.update({ solutionId }, { $set: updatedSolution }).then(() => {
				// update the solution in cache:
				client.setex(solutionId, process.env.CACHE_TIME, JSON.stringify(updatedSolution));
				// remove the attach file buffer
				updatedSolution.files.forEach((info) => { delete info.data });

				// Reset all solutions key in cache
				// because solution is updated successfully.
				Solution.find({},{ title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1}).then(solutions => {
					client.setex('allSolutions', process.env.CACHE_TIME, JSON.stringify(solutions));
				})
					.then(() => res.status(200).json({ solution: updatedSolution }));
			})
				.catch(err => res.status(400).json({ errors: { global: "Solution Draft Update error" } }));
		}
		else {
			res.status(400).json({ errors: { global: "no solution is found" } })
		}
	})
		.catch(err => {
			res.status(400).json({ errors: { global: "Incorrect solution Id" } });
		});
})

// Update solution by solution id
router.put('/:solutionId', (req, res) => {
	const fullName = req.currentUser.firstName + " " + req.currentUser.lastName;
	const { solutionId } = req.params;

	let updatedSolution = req.body.solution;
	if (fullName.length > 0) {
		updatedSolution.updatedUsername = fullName;
	}

	Solution.findOne({ solutionId }).then(solution => {
		if (solution) {
			updatedSolution.files = solution.files;
			Solution.update({ solutionId }, { $set: updatedSolution }).then(() => {
				// remove the attach file buffer
				updatedSolution.files.forEach((info) => { delete info.data });

				// update the solution in cache:
				client.setex(solutionId, process.env.CACHE_TIME, JSON.stringify(updatedSolution));
				
				// Reset all solutions key in cache
				// because solution is updated successfully.
				Solution.find({},{ title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1})
				.then(solutions => {
					client.setex('allSolutions', process.env.CACHE_TIME, JSON.stringify(solutions));
				})
					.then(() => res.status(200).json({ solution: updatedSolution }));

			}).catch(err => res.status(400).json({ errors: { global: "Update solution error" } }));
		}
		else {
			res.status(400).json({ errors: { global: "no solution is found" } })
		}
	})
		.catch(err => {
			res.status(400).json({ errors: { global: "Incorrect solution Id" } });
		});

})

// Delete Solution by Id and decrement Solution Id
router.delete('/:solutionId', (req, res) => {
	const { solutionId } = req.params;

	Solution.findOne({ solutionId }).then(solution => {
		Solution.remove({ 'solutionId': solutionId }).then((response) => {
			if (response.ok === 1) {
				// remove the solution in cache:
				client.del(solutionId);

				// Send Delete Solution Email to admin with email notification turn on:
				User.find({ $and: [{ role: process.env.SEND_EMAIL_ROLE }, { notification: true }] })
					.then(users => {
						_.map(users, async (user) => {
							await sendDeleteSolutionEmail(user, solution);
						});
					});
		
				// Reset all solutions key in cache because a new solution is created.
				Solution.find({},{ title: 1, solutionId: 1, status: 1, visibility: 1, topic: 1})
				.then(solutions => {
					client.setex('allSolutions',
						process.env.CACHE_TIME, JSON.stringify(solutions));
				});
	
				res.status(200).json({});
			}
			else {
				res.status(400).json({ errors: { global: `Failed to delete solution with Id ${solutionId}` } });
			}
		})
	})
	.catch(err => {
		res.status(400).json({ errors: { global: "Incorrect solution Id" } });
	});
});

// Update Solution by uploading files:
router.put('/:solutionId/attachment', upload.single('uploadFile'), (req, res) => {
	const { solutionId } = req.params;

	let fileInfo = {
		name: req.file.originalname,
		mimetype: req.file.mimetype,
		size: req.file.size,
		data: req.file.buffer,
		uploadDate: Date.now()
	};

	Solution.findOne({ solutionId }).then(solution => {
		if (solution) {
			solution.files.push(fileInfo);
			solution.save();
			res.status(200).json({ solution });
		}
		else {
			res.status(400).json({ errors: { global: "Invalid solution Id" } })
		}
	}).
	catch(err => {
		console.log(err);
		res.status(500).json({ errors: { global: err } })
	});

})

// Upload Screenshot to server:
router.post('/image', upload.single('uploadFile'), (req, res) => {
	const mimeType = req.file.mimetype;
	if (!mimeType.startsWith('image')) {
		res.status(400).json({ errors: { global: "We only accept image in the content." } })
	}
	else {
		const fileType = mimeType.substring(6);
		const buf = new Buffer(req.file.buffer.toString('base64'), 'base64');

		const imageFolder = path.join(__dirname, '../../../knowledgebase-react/public/', 'inlineImages');
		const imageFilename = `${new Date().getTime()}.${fileType}`;
		const imageFilePath = path.join(imageFolder, imageFilename);
		const returnFilePath = `/inlineImages/${imageFilename}`;

		if (!fs.existsSync(imageFolder)) {
			fs.mkdirSync(imageFolder);
		}
		fs.writeFile(`${imageFilePath}`, buf, 'binary')
			.then(res.status(200).json({ url: returnFilePath }));
	}
})

// Download attachment by solution Id and filename
router.get('/:solutionId/attachment', (req, res) => {
	const { filename } = req.query;
	const { solutionId } = req.params;
	
	Solution.findOne({ solutionId }).then(solution => {
		if (solution) {
			const { name, data, mimetype } = solution.files.find(item => item.name === filename);
			const dataString = data.toString('base64');
			res.status(200).json({ string: dataString, type: mimetype });
		}
		else {
			res.status(400).json({ errors: { global: "Invalid solution Id" } })
		}
	})
})

// Download all attachments by solution Id:
router.get('/:solutionId/attachments', (req, res) => {
	const { solutionId } = req.params;

	Solution.findOne({ solutionId }).then(solution => {
		if (solution) {
			const uploadDir = `uploads/solution_${solutionId}`;
			const files = solution.files;
			if (files.length > 0) {
				if (!fs.existsSync(uploadDir)) {
					fs.mkdirSync(uploadDir);
				}

				let promises = [];
				files.forEach((file) => {
					const buf = new Buffer(file.data.toString('base64'), 'base64');
					const promise = fs.writeFile(`${uploadDir}/${file.name}`, buf, 'binary');
					promises.push(promise);
				});

				Promise.all(promises).then((data) => {
					const zipFileName = `${uploadDir}_attachments.zip`;
					zipFolder(uploadDir, zipFileName, function (err, data) {
						if (err) {
							res.status(400).json({ errors: { global: "Zip Files Error" } })
						}
						else {
							fs.removeSync(uploadDir);
							fs.readFile(zipFileName, function (err, data) {
								if (err) {
									res.status(400).json({ errors: { global: "Read Zip File error" } });
								}
								else {
									fs.removeSync(zipFileName);
									const dataString = data.toString('base64');
									res.status(200).json({
										data: dataString,
										filename: "attachments.zip",
										mimetype: "application/x-zip-compressed"
									})
								}
							})
						}
					})
				})
					.catch(err => res.status(400).json({ errors: { global: err } }));
			}
			else {
				res.status(400).json({ errors: { global: "No attached file is found" } })
			}
		}
		else {
			res.status(400).json({ errors: { global: "Invalid solution Id" } })
		}
	})
})

export default router;
