import express from 'express';
import path from 'path';

const router = express.Router();

router.get('/', (req, res) => {
  const imageFilename = req.query.n;
  const imageFolder = path.join(__dirname, '../', 'inlineImages');
  const imageFilePath = path.join(imageFolder, imageFilename);
  res.sendFile(imageFilePath);
})

export default router;