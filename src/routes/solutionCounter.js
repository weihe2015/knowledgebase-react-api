import express from 'express';
import authenticate from '../middlewares/authenticate';
import SolutionCounter from '../models/SolutionCounter';

const router = express.Router();
router.use(authenticate);

router.get('/solutionId', (req, res) => {
  SolutionCounter.count().then((count) => {
    if (count === 0){
      SolutionCounter.create({
          solutionId: process.env.SOLUTION_ID,
          seq: process.env.SOLUTION_STARTING_NUM
      }).then(data => res.status(200).json({counter: data.seq}))
      .catch(err => res.status(400).json({errors: {global: "Empty solution set, Invalid Solution Credential"}}));
    }
    else {
      SolutionCounter.findOne({solutionId: process.env.SOLUTION_ID})
        .then((data) => res.status(200).json({counter: data.seq}))
        .catch(err => res.status(400).json({errors: {global: "Invalid Solution Credential"}}));
    }
  });
})

export default router;
