import express from 'express';
import jwt from 'jsonwebtoken';
import CryptoJS from "crypto-js";
import Validator from 'validator';
import generator from 'generate-password';
import User from '../models/User';
import parseErrors from '../utils/parseErrors';
import { sendConfirmationEmail,
	 sendSSOUserConfirmationEmail,
	 sendResetPasswordEmail,
	 sendResetPasswordConfirmationEmail,
	 sendChangePasswordConfirmationEmail,
	 sendChangeUserInfoConfirmationEmail
} from '../mailer';

const router = express.Router();

// Login with username or email, along with valid password
router.post('/login', (req, res) => {
	const { ciphertext } = req.body;
	const bytes = CryptoJS.AES.decrypt(ciphertext, process.env.CRYPTOJS_SECRET);
  	const credentials = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

	// Handle user when user login with email:
	if (Validator.isEmail(credentials.email)) {
		User.findOne({ email: credentials.email }).then(user => {
			if (user) {
				if (user.isSSOUser()) {
					res.status(403).json({errors: {global: "TPT Auth user"}});
				}
				else if (!user.isValidPassword(credentials.password)) {
					res.status(401).json({errors: {global: "Invalid credentials"}});
				}
				else if (!user.isConfirmed()) {
					res.status(400).json({errors: {global: "Your account is not verified."}});
				}
				else {
					res.status(201).json({user: user.toAuthJSON() });
				}
			}
			else {
				res.status(404).json({errors: {global: "Invalid credential: Email not found"}});
			}
		});
	}
	else {
		User.findOne({ username: credentials.email }).then(user => {
			if (user) {
				if (user.isSSOUser()) {
					res.status(403).json({errors: {global: "TPT Auth user"}});
				}
				else if (!user.isValidPassword(credentials.password)) {
					res.status(401).json({errors: {global: "Invalid credentials"}});
				}
				else if (!user.isConfirmed()) {
					res.status(400).json({errors: {global: "Your account is not verified."}});
				}
				else {
					res.status(201).json({user: user.toAuthJSON() });
				}
			}
			else {
				res.status(404).json({errors: {global: "Invalid credentials: Username not found"}});
			}
		});
	}
});

// TPT Auth: Fetch Redirect URL:
router.get('/oauth', (req, res) => {
	const sessionId = req.sessionID;
	let host = '';
	if (process.env.SYSTEM_ENV === 'production') {
		host = 'https%3A%2F%2Fglsupport-kb.translations.com';
	}
	else {
		host = 'http%3A%2F%2Flocalhost:3000';
	}
	const clientId = 'wJfzP8TpMXbWYNUy4Vhn59AGR63rHvdg';
	const redirectURL = `https://sso.transperfect.com/connect/authorize?client_id=${clientId}&client_uri=${host}&nonce=${sessionId}&redirect_uri=${host}%2FSSO&response_type=id_token%20token&scope=openid%20email%20profile`;
	res.status(200).json({ url: redirectURL });
});

// TPT Auth: Login with email address:
router.post('/oauthlogin', (req, res) => {
	const { ciphertext } = req.body;
	const bytes = CryptoJS.AES.decrypt(ciphertext, process.env.CRYPTOJS_SECRET);
  	const credentials = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

	User.findOne({ email: credentials.email }).then(user => {
		if (user) {
			// no need to check password because user has login with TPT auth:
			if (!user.isConfirmed()) {
				res.status(400).json({ errors: { global: "Your account is not verified" }});
			}
			else {
				res.status(201).json({ user: user.toAuthJSON() });
			}
		}
		else {
			res.status(404).json({ errors: {global: "User's email is not found. Please register with admin user first."}});
		}
	});

});

// Create a new User
router.post('/user', (req, res) => {
	const { ciphertext } = req.body;
	const bytes = CryptoJS.AES.decrypt(ciphertext, process.env.CRYPTOJS_SECRET);
	const info = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
	const { firstName, lastName, username, role, email, products, SSOUser } = info;
	const tempPassword = generator.generate({
    		length: 12,
    		numbers: true
	});
	const user = new User({ firstName, lastName, username, role, email, products });
	user.setPassword(tempPassword);
	user.setConfirmationToken();
	// Initially all user does not have email notification
	user.setNotification(false);
	// set SSOUser boolean for User Object
	user.setSSOUser(SSOUser);
	user.save().then(userRecord => {
		if (userRecord.SSOUser) {
			sendSSOUserConfirmationEmail(userRecord);
		}
		else {
			sendConfirmationEmail(userRecord, tempPassword);
		}
		res.status(201).json({user: userRecord.toAuthJSON()});
	})
	.catch(err => {
		res.status(400).json({errors: parseErrors(err.errors)});
	});
});

// confirm user to finish registeration
router.put('/user/token', (req, res) => {
	const token = req.body.token;

	User.findOne({ confirmationToken: token })
	.then(user => {
		if (user) {
			if (user.confirmed) {
				res.status(409).json({errors: {global: "This user has been confirmed already."} });
			}
			else {
					User.findOneAndUpdate(
						{ confirmationToken: token },
						{ confirmationToken: "", confirmed: true },
						{ new: true }
					).then( user => res.json({ user: user.toAuthJSON() }))
			}
		}
		else {
			res.status(401).json({errors: {global: "Invalid Confirmation Token"} })
		}
	})
});

// Delete User by UserId
router.delete('/user/:userId', (req, res) => {
        const _id = req.params.userId;

	User.deleteOne({ _id }).then(response => {
		if (response.ok === 1) {
			res.status(201).json({});
		}
		else {
			res.status(400).json({ errors: {global: "Failed to delete a user"}});
		}
	}).catch(err => {
		res.status(500).json({ errors: { global: err } });
	});
})

// Send user forgot password email
router.get('/user/password', (req, res) => {
	const email  = req.query.em;
	User.findOne({email: email}).then(user => {
		if (user) {
			sendResetPasswordEmail(user);
			res.status(200).json({});
		}
		else {
			res.status(404).json({ errors: {global: "We do not recognize your credentials."}});
		}
	})
});

// Validate User Token:
router.get('/user/token/:userToken', (req, res) => {
	const token = req.params.userToken;
	jwt.verify(token, process.env.JWT_SECRET, err => {
		if (err) {
			res.status(401).json({ errors: { global: "Token has been expired."} });
		}
		else {
			res.status(200).json({ res: "Token is valid." });
		}
	})
});

// Reset User password
router.post('/user/password', (req, res) => {
	const { ciphertext } = req.body;
	const bytes = CryptoJS.AES.decrypt(ciphertext, process.env.CRYPTOJS_SECRET);
	const data = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
	const { password, token } = data;
	jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
		if (err) {
			res.status(401).json({ errors: { global: "Invalid Reset Password Token"} });
		}
		else {
			User.findOne({ _id: decoded._id }).then(user => {
				if (user) {
					if (user.isValidPassword(password)) {
						res.status(400).json({ errors: { passwordConfirmation:
							"New Password cannot be the same as old password. Please try again." }});
					}
					else {
						user.setPassword(password);
						user.save().then(() => res.status(200).json({ }));
						sendResetPasswordConfirmationEmail(user);
					}
				}
				else {
					res.status(404).json({ errors: { global: "Invalid Reset Password Token" } });
				}

			})
		}
	})
});

// fetch all user info:
router.get('/users', (req, res) => {
	const token  = req.query.t;
	jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
		if (err) {
			res.status(401).json({ errors: { global: "Invalid fetch user Token"} });
		}
		else {
			User.find().then((users) => {
				res.status(200).json(users);
			});
		}
	});
});

// Change User's Password, by passing encrypted user previous and new password as body
router.put('/user/password', (req, res) => {
	const { ciphertext } = req.body;
	const bytes = CryptoJS.AES.decrypt(ciphertext, process.env.CRYPTOJS_SECRET);
	const data = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
  	const { token, currentPassword, password } = data;

	jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
		if (err) {
			res.status(401).json({ errors: { global: "Invalid Reset Password Token"} });
		}
		else {
			User.findOne({ email: decoded.email }).then(user => {
				if (user) {
					if (user.isValidPassword(currentPassword)) {
						user.setPassword(password);
						user.save().then(() => res.status(200).json({}));
						sendChangePasswordConfirmationEmail(user);
					}
					else {
						res.status(400).json({ errors: { currentPassword:
							"Current Password is not valid. Please try again." }});
					}
				}
				else {
					res.status(404).json({ errors: { global: "Invalid Reset Password Token" } });
				}
			})
		}
	});
});

// Fetch User by User Id:
router.get('/user', (req, res) => {
	const email  = req.query.em;
	// Handle admin get user by email:
	if (Validator.isEmail(email)) {
		User.findOne({ email }).then(user => {
			if(user) {
				res.status(200).json({user: user.toAuthJSON() });
			}
			else {
				res.status(400).json({errors: {global: "Cannot find user info with this email: " + email}});
			}
		});
	}
	// Handle admin get user by username:
	else {
		User.findOne({ username: email }).then(user => {
			if(user) {
				res.status(200).json({user: user.toAuthJSON() });
			}
			else {
				res.status(400).json({errors: {global: "Cannot find user info with this username: " + email}});
			}
		});
	}
});

// Change User Info, Use Id as Parameter to pass to this API:
router.put('/user/:userId', (req, res) => {
        const _id = req.params.userId;
	const { ciphertext } = req.body;
	const bytes = CryptoJS.AES.decrypt(ciphertext, process.env.CRYPTOJS_SECRET);
	const credentials = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

	// Find User by User Id and update the Info:
	User.findOne({ _id }).then(user => {
		if (user) {
			if (credentials.firstName.length > 0) {
				user.setFirstName(credentials.firstName);
			}
			if (credentials.lastName.length > 0) {
				user.setLastName(credentials.lastName);
			}
			if (credentials.role.length > 0) {
				user.setRole(credentials.role);
			}
			if (credentials.password.length > 0) {
				user.setPassword(credentials.password);
			}
			if (user.role === 'admin') {
				user.setNotification(credentials.notification);
			}
			if (user.role === 'client') {
				if (credentials.products.length > 0) {
					user.setProducts(credentials.products);
				}
			}
			// set SSO Option for User:
			user.setSSOUser(credentials.SSOUser);

			user.save().then(() => res.status(201).json({}));
			if (credentials.password.length > 0) {
				sendChangePasswordConfirmationEmail(user);
		        }
			else {
		 		sendChangeUserInfoConfirmationEmail(user);
			}
		}
		else {
			res.status(404).json({errors: { global: "Cannot find user info" }});
		}
	});

});

export default router;
