import express from 'express';
import jwt from 'jsonwebtoken';
import Product from '../models/Product';
import CryptoJS from 'crypto-js';
import parseErrors from '../utils/parseErrors';
import authenticate from '../middlewares/authenticate';

const router = express.Router();
router.use(authenticate);

// add new Product:
router.post('/', (req, res) => {
	const { ciphertext } = req.body;
	const bytes = CryptoJS.AES.decrypt(ciphertext, process.env.CRYPTOJS_SECRET);
	const info = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
	const { productName } = info;
	if (productName === undefined || productName.length === 0) {
		res.status(400).json({ errors: { global: "productName cannot be empty." }});
	}
	else {
		const product = new Product({ productName });
		product.save().then(() => {
			// fetch all products:
			Product.find().then(products => {
				let topicOptions = [];
				let clientProductOptions = [];
				products.forEach(product => {
					topicOptions.push(product.generateTopicOptions());
					clientProductOptions.push(product.generateClientProductOptions());
				});
				topicOptions.sort((a, b) => (a.value > b.value) ? 1 : -1);
				clientProductOptions.sort((a, b) => (a.value > b.value) ? 1 : -1);
				products.sort((a, b) => (a.productName > b.productName) ? 1 : -1);
				res.status(201).json({ topicOptions, clientProductOptions, products });
			});
		}).catch(err => {
			res.status(400).json({ errors: parseErrors(err.errors)})
		})
	}
});

// modify existing product:
router.put('/:productId', (req, res) => {
	const _id = req.params.productId;
	const { ciphertext } = req.body;
	const bytes = CryptoJS.AES.decrypt(ciphertext, process.env.CRYPTOJS_SECRET);
	const info = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
	const { productName } = info;
	
	// Find Product by ProductId and update the info:
	Product.findOne({ _id }).then(product => {
		if (product) {
			product.setProductName(productName);
			product.save().then(() => {
				// fetch all products:
				Product.find().then(products => {
					let topicOptions = [];
					let clientProductOptions = [];
					products.forEach(product => {
						topicOptions.push(product.generateTopicOptions());
						clientProductOptions.push(product.generateClientProductOptions());
					});
					topicOptions.sort((a, b) => (a.value > b.value) ? 1 : -1);
					clientProductOptions.sort((a, b) => (a.value > b.value) ? 1 : -1);
					products.sort((a, b) => (a.productName > b.productName) ? 1 : -1);
					res.status(201).json({ topicOptions, clientProductOptions, products });
				})
			});
		}
		else {
			res.status(404).json({ errors: { global: "Cannot find product info" }});
		}
	});
});

// delete existing product:
router.delete('/:productId', (req, res) => {
	const _id = req.params.productId;
	Product.deleteOne({ _id }).then(response => {
		if (response.ok === 1) {
			// refetch all products:
			Product.find().then(products => {
				let topicOptions = [];
				let clientProductOptions = [];
				products.forEach(product => {
					topicOptions.push(product.generateTopicOptions());
					clientProductOptions.push(product.generateClientProductOptions());
				});
				topicOptions.sort((a, b) => (a.value > b.value) ? 1 : -1);
				clientProductOptions.sort((a, b) => (a.value > b.value) ? 1 : -1);
				products.sort((a, b) => (a.productName > b.productName) ? 1 : -1);
				res.status(201).json({ topicOptions, clientProductOptions, products });
			});
		}
		else {
			res.status(400).json({errors: { global: "Failed to delete a product" }});
		}
	}).catch(err => {
			res.status(400).json({ errors: parseErrors(err.errors)})
	});
});

// fetch all existing products:
router.get('/', (req, res) => {
	Product.find().then(products => {
		let topicOptions = [];
		let clientProductOptions = [];
		products.forEach(product => {
			topicOptions.push(product.generateTopicOptions());
			clientProductOptions.push(product.generateClientProductOptions());
		});
		topicOptions.sort((a, b) => (a.value > b.value) ? 1 : -1);
		clientProductOptions.sort((a, b) => (a.value > b.value) ? 1 : -1);
		products.sort((a, b) => (a.productName > b.productName) ? 1 : -1);
		res.status(200).json({ topicOptions, clientProductOptions, products });
	}).catch(err => {
		res.status(400).json({ errors: parseErrors(err.errors)});
	})
})

export default router;
