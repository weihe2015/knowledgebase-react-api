import nodemailer from 'nodemailer';

function devSetup() {
	return nodemailer.createTransport({
		host: process.env.EMAIL_HOST,
		port: process.env.EMAIL_PORT,
		auth: {
			user: process.env.EMAIL_USER,
			pass: process.env.EMAIL_PASS
		}
	});
}

function prodSetup() {
	return nodemailer.createTransport({
		sendmail: true,
		newline: 'unix',
		path: '/usr/sbin/sendmail'
	});
}

export function sendConfirmationEmail(user, tempPassword) {
	const transport = process.env.SYSTEM_ENV === 'development' ? devSetup() : prodSetup();
	const email = {
		from: process.env.FROM_EMAIL,
		to: user.email,
		subject: "Welcome to Translations.com Knowledge Base System",
		html: `
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		Dear <b>${user.firstName} ${user.lastName}</b>,<br /><br />

		<p>Welcome to Translations.com KnowledgeBase System.<br />
		Please confirm your email with following button: <br/><br/>
		<a class="btn btn-info" href="${user.generateConfirmationUrl()}"
			role="button">Confirm Email</a>

		<br/>
		<p>Your username: ${user.username}, email address: ${user.email}. You may use either username or email to login.</p>
		<p>Your temporary password is ${tempPassword}. Please change it immediately after login.</p>
		<p>Please do not reply to this email.</p>
		<br /><br />
		Thank you,<br />
		The Translations.com Knowledge Base Team
		</p>
		`
	}
	transport.sendMail(email, (err, info) => {
		if (err) {
			console.log(err);
		}
	});
}

export function sendSSOUserConfirmationEmail(user) {
	const transport = process.env.SYSTEM_ENV === 'development' ? devSetup() : prodSetup();
	const email = {
		from: process.env.FROM_EMAIL,
		to: user.email,
		subject: "Welcome to Translations.com Knowledge Base System",
		html: `
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		Dear <b>${user.firstName} ${user.lastName}</b>,<br /><br />

		<p>Welcome to Translations.com KnowledgeBase System.<br />
		Please confirm your email with following button: <br/><br/>
		<a class="btn btn-info" href="${user.generateConfirmationUrl()}"
			role="button">Confirm Email</a>

		<br/>
		<p>You may login with your domain credentials with TPT Auth after confirmation.</p>
		<p>Please do not reply to this email.</p>
		<br /><br />
		Thank you,<br />
		The Translations.com Knowledge Base Team
		</p>
		`
	}
	transport.sendMail(email, (err, info) => {
		if (err) {
			console.log(err);
		}
	});
}

export function sendResetPasswordEmail(user) {
	const transport = process.env.SYSTEM_ENV === 'development' ? devSetup() : prodSetup();
	const email = {
		from: process.env.FROM_EMAIL,
		to: user.email,
		subject: "Reset Password",
		html: `
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		Dear <b>${user.firstName} ${user.lastName}</b>,<br /><br />

		<p>To reset password, please follow the link below:<br /><br />
		<a class="btn btn-info"
			href="${user.generateResetPasswordUrl()}" role="button">Reset Password</a>
		<br/>
		<p>Please do not reply to this email.</p>
		<br />
		Thank you,<br />
		The Translations.com Knowledge Base Team
		</p>
		`
	}
	transport.sendMail(email, (err, info) => {
		if (err) {
			console.log(err);
		}
	});
}

export function sendResetPasswordConfirmationEmail(user) {
	const transport = process.env.SYSTEM_ENV === 'development' ? devSetup() : prodSetup();
	const email = {
		from: process.env.FROM_EMAIL,
		to: user.email,
		subject: "Reset Password Confirmation",
		html: `
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

		Dear <b>${user.firstName} ${user.lastName}</b>,<br /><br />

		<p>You have just reset your password. If you have not done so, please contact
			<a class="btn btn-info"
				href="mailto:support@translations.com">support@translations.com</a> immediately.
		<br/>
	  	<p>Please do not reply to this email.</p>
		<br />
		Thanks you,<br />
		The Translations.com Knowledge Base Team</p>`
	}
	transport.sendMail(email, (err, info) => {
		if (err) {
			console.log(err);
		}
	});
}

export function sendChangePasswordConfirmationEmail(user) {
	const transport = process.env.SYSTEM_ENV === 'development' ? devSetup() : prodSetup();
	const email = {
		from: process.env.FROM_EMAIL,
		to: user.email,
		subject: "Change Password Confirmation",
		html: `
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

		Dear <b>${user.firstName} ${user.lastName}</b>,<br /><br />

		<p>You have just changed your password. If you have not done so, please contact
			<a class="btn btn-info"
				href="mailto:support@translations.com">support@translations.com</a> immediately.
	  	<br/>
	  	<p>Please do not reply to this email.</p>
		<br />
		Thank you,<br />
		The Translations.com Knowledge Base Team</p>`
	}
	transport.sendMail(email, (err, info) => {
		if (err) {
			console.log(err);
		}
	});
}


export function sendChangeUserInfoConfirmationEmail(user) {
	const transport = process.env.SYSTEM_ENV === 'development' ? devSetup() : prodSetup();
	const email = {
		from: process.env.FROM_EMAIL,
		to: user.email,
		subject: "Change User Info Confirmation",
		html: `
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

		Dear <b>${user.firstName} ${user.lastName}</b>,<br /><br />

		<p>You have just changed your account Info. If you have not done so, please contact
			<a class="btn btn-info"
				href="mailto:support@translations.com">support@translations.com</a> immediately.
	  	<br/>
	  	<p>Please do not reply to this email.</p>
		<br />
		Thank you,<br />
		The Translations.com Knowledge Base Team</p>`
	}
	transport.sendMail(email, (err, info) => {
		if (err) {
			console.log(err);
		}
	});
}

export function sendUnApprovedSolutionEmail(user, solution) {
	const transport = process.env.SYSTEM_ENV === 'development' ? devSetup() : prodSetup();
	const email = {
		from: process.env.FROM_EMAIL,
		to: user.email,
		subject: `Solution: ${solution.solutionId} under ${solution.topic}, status: Unapproved`,
		html: `
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

		Dear <b>${user.firstName} ${user.lastName}</b>,<br /><br />

		<p>The solution with title: "${solution.title}" and topic: "${solution.topic}"
			has been created and is waiting approval.
			Please follow the link below to login to the Knowledge Base System
			and click "Approve" or "Reject".<br/><br/>
			<a class="btn btn-info"
				href="${process.env.HOST}/solutions/view?solutionId=${solution.solutionId}&title=${solution.title}">
				View Solution</a>
		<br/>
		<p>Please do not reply to this email.</p>
		<br />
		Thank you,<br />
		The Translations.com Knowledge Base Team</p>`
	}
	transport.sendMail(email, (err, info) => {
		if (err) {
			console.log(err);
		}
	});
}

export function sendApprovalSolutionEmail(user, solution, approvalComment) {
	const transport = process.env.SYSTEM_ENV === 'development' ? devSetup() : prodSetup();
	const email = {
		from: process.env.FROM_EMAIL,
		to: user.email,
		subject: `Solution: ${solution.solutionId} under ${solution.topic}, status: Approved`,
		html: `
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

		Dear <b>${user.firstName} ${user.lastName}</b>,<br /><br />

		<p>The solution with title: "${solution.title}" and topic: "${solution.topic}"
			has been approved.<br/><br/>
			<a class="btn btn-info"
				href="${process.env.HOST}/solutions/view?solutionId=${solution.solutionId}&title=${solution.title}">
				View Solution</a>
		<br/>
		<p>Approval Comment: "${approvalComment}."</p>
		<br/>
		<p>Please do not reply to this email.</p>
		<br />
		Thank you,<br />
		The Translations.com Knowledge Base Team</p>`
	}
	transport.sendMail(email, (err, info) => {
		if (err) {
			console.log(err);
		}
	});
}

export function sendRejectSolutionEmail(user, solution, rejectionComment) {
	const transport = process.env.SYSTEM_ENV === 'development' ? devSetup() : prodSetup();
	const email = {
		from: process.env.FROM_EMAIL,
		to: user.email,
		subject: `Solution: ${solution.solutionId} under ${solution.topic}, status: Rejected`,
		html: `
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

		Dear <b>${user.firstName} ${user.lastName}</b>,<br /><br />

		<p>The solution with title: "${solution.title}" and topic: "${solution.topic}"
			has been rejected.<br/><br/>
			<a class="btn btn-info"
				href="${process.env.HOST}/solutions/view?solutionId=${solution.solutionId}&title=${solution.title}">
				View Solution</a>
		<br/>
		<p>Rejection Comment: "${rejectionComment}."</p>
		<br/>
		<p>Please do not reply to this email.</p>
		<br />
		Thank you,<br />
		The Translations.com Knowledge Base Team</p>`
	}
	transport.sendMail(email, (err, info) => {
		if (err) {
			console.log(err);
		}
	});
}

export function sendDeleteSolutionEmail(user, solution) {
	const transport = process.env.SYSTEM_ENV === 'development' ? devSetup() : prodSetup();
	const email = {
		from: process.env.FROM_EMAIL,
		to: user.email,
		subject: `Solution: ${solution.solutionId} under ${solution.topic}, status: Deleted`,
		html: `
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

		Dear <b>${user.firstName} ${user.lastName}</b>,<br /><br />

		<p>The solution with title: "${solution.title}" and topic: "${solution.topic}"
			has been deleted.<br/><br/>
		<p>Please do not reply to this email.</p>
		<br />
		Thank you,<br />
		The Translations.com Knowledge Base Team</p>`
	}
	transport.sendMail(email, (err, info) => {
		if (err) {
			console.log(err);
		}
	});
}
