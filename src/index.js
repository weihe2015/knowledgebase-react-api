import express from 'express';
import session from 'express-session';
import path from 'path';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
import Promise from 'bluebird';
import auth from './routes/auth';
import solutions from './routes/solutions';
import solutionCounter from './routes/solutionCounter';
import products from './routes/products';
import image from './routes/image';
import moment from 'moment';

dotenv.config();
const app = express();
mongoose.Promise = Promise;
mongoose.connect(process.env.MONGODB_URL);
/*
const credentials = {
	client: {
		id: 'wJfzP8TpMXbWYNUy4Vhn59AGR63rHvdg',
		secret: 'VYf53KMD6gPuJR9HSbk4x2U8GChEnL7c'
	},
	auth: {
		tokenHost: 'https://sso-stg.transperfect.com',
		authorizePath: '/connect/authorize'
	}
};

const oauth2 = require('simple-oauth2').create(credentials);

// Authorization oauth2 URI
const authorizationUri = oauth2.authorizationCode.authorizeURL({
   redirect_uri: 'http://localhost:3000/SSO',
   scope: 'openid email profile',
});
*/
// serve the react app files:
app.use(express.static(`/opt/html`));
// Add user session and secrete token of Open ID connect
app.use(session({
	cookie: { httpOnly:  true },
	secret: "VYf53KMD6gPuJR9HSbk4x2U8GChEnL7c",
	resave: true,
	saveUninitialized: true
}));

app.use(bodyParser.json({ limit: '100mb', extended: true }));
//app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use('/api/v1/auth/', auth);
app.use('/api/solutions', solutions);
app.use('/api/solutionCounter', solutionCounter);
app.use('/api/v1/products', products);
//app.use('/inlineImages', image);

app.get('/inlineImages/*', (req, res) => {
	res.sendFile(path.join(__dirname, req.originalUrl));
})

app.get('/*', (req, res) => {
	res.sendFile(path.join(__dirname, 'index.html'));
})

let port = process.env.PORT || 7010;
if (process.env.SYSTEM_ENV === 'development') {
	port = 7020;
}

app.listen(port, () => console.log(`${moment().format("YYYY-MM-DD hh:mm A")} Running on ${port}`));
