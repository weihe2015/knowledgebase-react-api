import jwt from 'jsonwebtoken';
import User from '../models/User';

const authenticate = (req, res, next) => {
	const header = req.headers.authorization;
	let token;

	if (header) {
		token = header.split(' ')[1];
	}

	if (token) {
		jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
			if (err) {
				res.status(401).json({ errors: { global: "Invalid token."} });
			}
			else {
				const email = decoded.email;
				User.findOne({ email }).then(user => {
					req.currentUser = user;
					next();
				})
			}
		})
	}
	else {
		res.status(401).json({ errors: { global: "No valid token."} });
	}
}

export default authenticate;
