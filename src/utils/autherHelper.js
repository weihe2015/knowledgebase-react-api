const clientId = 'wJfzP8TpMXbWYNUy4Vhn59AGR63rHvdg';
const clientSecret = 'VYf53KMD6gPuJR9HSbk4x2U8GChEnL7c';
const redirectUri = 'http://localhost:3000/SSO';
const scopes = [
	'openid',
	'username',
	'email',
	'profile',
	'phone'
];

const credentials = {
	client: {
		id: clientId,
		secret: clientSecret
	},
	auth: {
		tokenHost: 'https://sso.transperfect.com/connect/authorize'
	}
};

const oauth2 = require('simple-oauth2').create(credentials);

export function getAuthUrl(){
	const returnVal = oauth2.authCode.authorizeURL({
		redirect_uri: redirectUri,
		scope: scopes.join('')
	});
	console.log('');
	console.log('Generated auth url: ' + returnVal);
	return returnVal;
}

export function getTokenFromCode(authCode, callback, request, response) {
	oauth2.authCode.getToken({
		code: authCode,
		redirect_uri: redirectUri,
		scope: scopes.join('')
	}, function(err, result) {
		if (err) {
			console.log('Access token error: ', err.message);
			callback(request, response, error, null);	
		}
		else {
			const token = oauth2.accessToken.create(result);
			console.log('Token Created: ', token.token);
			callback(request, response, null, token);
		}
	});
}


