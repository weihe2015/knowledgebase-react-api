import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import uniqueValidator from 'mongoose-unique-validator';

const schema = new mongoose.Schema({
	firstName: {
		type: String,
		required: true
	},
	lastName: {
		type: String,
		required: true
	},
	username: {
		type: String,
		required: true,
		index: true,
		unique: true
	},
	role: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true,
		lowercase: true,
		index: true,
		unique: true
	},
	passwordHash: {
		type: String,
		required: true
	},
	products: {
		type: [String],
		required: false
	},
	notification: {
	 	type: Boolean,
   		default: false
	},
	confirmed: {
		type: Boolean,
		default: false
	},
	confirmationToken: {
		type: String,
		default: ''
	},
	SSOUser: {
		type: Boolean,
		default: false
	}
}, {timestamps: true});

schema.methods.isValidPassword = function isValidPassword(password) {
	return bcrypt.compareSync(password, this.passwordHash);
}

schema.methods.setPassword = function setPassword(password) {
	this.passwordHash = bcrypt.hashSync(password, 10);
}

schema.methods.isConfirmed = function isConfirmed() {
	return this.confirmed;
}

schema.methods.setRole = function setRole(newRole) {
	this.role = newRole;
}

schema.methods.setFirstName = function setFirstName(newFirstName) {
	this.firstName = newFirstName;
}

schema.methods.setLastName = function setLastName(newLastName) {
	this.lastName = newLastName;
}

schema.methods.setConfirmationToken = function setConfirmationToken() {
	this.confirmationToken = this.generateJWT();
}

schema.methods.setProducts = function setProducts(newProducts) {
	this.products = newProducts;
}

schema.methods.setNotification = function setNotification(newNotification) {
	this.notification = newNotification;
} 

schema.methods.setSSOUser = function setSSOUser(newSSOUser) {
	this.SSOUser = newSSOUser;
}

schema.methods.isSSOUser = function isSSOUser() {
	return this.SSOUser;
}

schema.methods.generateConfirmationUrl = function generateConfirmationUrl() {
	return `${process.env.HOST}/confirmation/${this.confirmationToken}`;
}

schema.methods.generateResetPasswordUrl = function generateResetPasswordUrl() {
	return `${process.env.HOST}/resetpassword/${this.generateResetPasswordToken()}`
}

schema.methods.generateJWT = function generateJWT() {
	return jwt.sign(
		{
			email: this.email,
			fullName: `${this.firstName} ${this.lastName}`,
			confirmed: this.confirmed,
			role: this.role,
			products: this.products
		},
		process.env.JWT_SECRET,
		{ expiresIn: "1d" }
	);
}

schema.methods.generateResetPasswordToken = function generateResetPasswordToken() {
	return jwt.sign(
		{
			_id: this._id
		},
		process.env.JWT_SECRET,
		{ expiresIn: "1h" }
	);
}

schema.methods.toAuthJSON = function toAuthJSON() {
	return {
		_id: this._id,
		email: this.email,
		fullName: `${this.firstName} ${this.lastName}`,
		confirmed: this.confirmed,
		role: this.role,
		products: this.products,
		SSOUser: this.SSOUser,
		token: this.generateJWT()
	}
};

schema.plugin(uniqueValidator,
	{message: "This field has been registered. Please try another one."});

export default mongoose.model('User', schema);
