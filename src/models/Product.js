import mongoose from 'mongoose';

const schema = new mongoose.Schema({
	productName: {
		type: String,
		required: true,
		index: true,
		unique: true
	}
}, {timestamps: true});

schema.methods.setProductName = function setProductName(productName) {
	this.productName = productName;
}

schema.methods.generateTopicOptions = function generateTopicOptions() {
	return { 
		value: this.productName,
		label: this.productName,
		text: this.productName  
	}
}

schema.methods.generateClientProductOptions = function generateClientProductOptions() {
	return {
		value: this.productName,
		key: this.productName,
		text: this.productName
	}
}

export default mongoose.model('Product', schema);
