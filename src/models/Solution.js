import mongoose from 'mongoose';

const schema = new mongoose.Schema({
	solutionId: {
		type: Number,
		required: true
	},
	title: {
		type: String,
		required: true
	},
	topic: {
		type: String,
		required: true
	},
	content: {
		type: String,
		required: true
	},
	visibility: {
		type: String
	},
	keywords: {
		type: String
	},
	status: {
		type: String
	},
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	},
	createdUsername: {
		type: String
	},
	updatedUsername: {
		type: String
	},
	expirationDate: {
		type: Date
	},
	viewNumber: {
		type: Number,
		required: true
	},
	ratingUp: {
		type: Number,
		required: true
	},
	ratingDown: {
		type: Number,
		required: true
	},
	ratingUserId: {
		type: Array
	},
	files: {
		type: Array
	},
	comments: {
		type: Array
	},
	history: {
		type: Array
	}
}, {timestamps: true});

schema.methods.setUpdatedUsername = function setUpdatedUsername(username) {
	this.updatedUsername = username;
}

schema.methods.updateFields = function updateFields(solution) {
	this.content = solution.content;
}

schema.methods.addUserIdToRatingArray = function addUserIdToRatingArray(userId) {
	this.ratingUserId.push(userId);
}

export default mongoose.model('Solution', schema);
