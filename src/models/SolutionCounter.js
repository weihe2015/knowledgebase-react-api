import mongoose from 'mongoose';

const schema = new mongoose.Schema({
  solutionId: {
    type: String,
    required: true
  },
  seq: {
    type: Number,
    required: true
  }
}, {timestamps: true});

schema.methods.incrementSequence = function incrementSequence() {
	this.seq++;
}

export default mongoose.model('SolutionCounter', schema);
